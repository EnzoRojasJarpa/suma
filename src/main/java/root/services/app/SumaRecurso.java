
package root.services.app;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
@Path("/")
public class SumaRecurso {
    @GET
    @Path("Suma")
    
    
    public String getSumaQuery(@QueryParam("Numeros")String numeros){
        String ListaNumeros[]=numeros.split(",");
        int suma=0;
        for (int i=0;i<ListaNumeros.length;i++) {
            suma=suma+ Integer.parseInt(ListaNumeros[i]);
        }
        
        return "La suma es: "+suma;
    }

}
